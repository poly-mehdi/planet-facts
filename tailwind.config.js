/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {},
    colors: {
      white: '#FFFFFF',
      black: '#070724',
      'dark-grey': '#38384F',
      grey: '#838391',
      cyan: '#419EBB',
      yellow: '#EDA249',
      purple: '#6F2ED6',
      'light-red': '#D14C32',
      red: '#D83A34',
      orange: '#CD5120',
      green: '#1EC2A4',
      blue: '#2D68F0',
    },
    fontFamily: {
      spartan: ['League Spartan', 'sans-serif'],
      antonio: ['Antonio', 'sans-serif'],
    },
  },
  plugins: [],
}
