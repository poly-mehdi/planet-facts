import data from './data.json';
import iconMenu from './assets/icon-hamburger.svg';
import iconSource from './assets/icon-source.svg';
import { useState } from 'react';
import Sidebar from './components/Sidebar';
import { useGlobalContext } from './context';

const App = () => {
  const [menu, setMenu] = useState('overview');
  const { isSidebarOpen, toggleSidebar, planet, setPlanet } =
    useGlobalContext();

  return (
    <main className="bg-black font-antonio">
      <header className="p-4 flex justify-between items-center mb-2 md:justify-center md:flex-col lg:flex-row lg:justify-between lg:px-4 py-4 lg:mb-20 lg:items-center ">
        <h2 className="uppercase text-white text-3xl md:py-10 md:mb-2 lg:py-0 lg:mb-0">
          The Planets
        </h2>
        <div className="gap-x-6 font-spartan text-2xl hidden md:flex lg:text-xl">
          {data.map((planet, item) => {
            return (
              <p
                className={`text-white cursor-pointer lg:text-grey ${
                  planet.name === data[item].name ? 'active-lg' : ''
                } active-lg`}
                key={item}
                onClick={() => {
                  setPlanet(data[item]);
                }}
              >
                {planet.name}
              </p>
            );
          })}
        </div>

        <img
          src={iconMenu}
          onClick={() => {
            toggleSidebar();
            isSidebarOpen
              ? (document.body.style.overflow = 'auto')
              : (document.body.style.overflow = 'hidden');
          }}
          alt="menu"
          className="h-5 cursor-pointer md:hidden"
        />
      </header>
      <Sidebar />
      <article>
        <section className="flex justify-between padding-menu items-center bordered h-14 md:hidden">
          <h3
            className={`uppercase text-white font-spartan tracking-widest  py-5 cursor-pointer ${
              menu === 'overview' ? 'active-menu' : ''
            }`}
            onClick={() => {
              setMenu('overview');
            }}
          >
            Overview
          </h3>
          <h3
            className={`uppercase text-white font-spartan tracking-widest py-5 cursor-pointer ${
              menu === 'structure' ? 'active-menu' : ''
            }`}
            onClick={() => {
              setMenu('structure');
            }}
          >
            Structure
          </h3>
          <h3
            className={`uppercase text-white font-spartan tracking-widest py-5 cursor-pointer ${
              menu === 'surface' ? 'active-menu' : ''
            }`}
            onClick={() => {
              setMenu('surface');
            }}
          >
            Surface
          </h3>
        </section>
        <section className="flex flex-col items-center px-4 image">
          {menu === 'overview' ? (
            <img
              src={planet.images.planet}
              alt="planet"
              className="my-24"
              style={{
                height: `${planet.size.height}`,
                width: `${planet.size.width}`,
              }}
            />
          ) : menu === 'structure' ? (
            <img
              src={planet.images.internal}
              alt="internal"
              className=" my-24"
              style={{
                height: `${planet.size.height}`,
                width: `${planet.size.width}`,
              }}
            />
          ) : (
            <div className="relative">
              <img
                src={planet.images.planet}
                alt="planet"
                className="my-24"
                style={{
                  height: `${planet.size.height}`,
                  width: `${planet.size.width}`,
                }}
              />
              <img
                src={planet.images.geology}
                alt="planet"
                className="w-20 h-24 h- my-24 z-10 absolute top-24 position-image"
              />
            </div>
          )}
        </section>
        <section className="description flex px-4 flex-col items-center md:items-start md:px-6">
          <h1 className="text-white uppercase text-5xl tracking-wide mb-6 text-center md:text-left lg:text-7xl">
            {planet.name}
          </h1>
          <p className="text-white font-spartan tracking-wide text-sm text-center mb-4 md:text-left  md:w-72 lg:w-96">
            {planet.overview.content}
          </p>
          <div className="flex gap-x-1 items-center mb-4">
            <p className="text-grey font-spartan text-md">
              Source :{' '}
              <a
                href={planet.overview.source}
                className="font-medium underline"
              >
                Wikipedia
              </a>
            </p>
            <img src={iconSource} alt="source" className="h-3 w-3" />
          </div>
        </section>
        <section className="menu flex flex-col gap-y-4 justify-center px-6 lg:w-5/6">
          {menu == 'overview' ? (
            <div
              className={`flex items-center gap-x-6 center pl-4`}
              style={{ backgroundColor: `${planet.color}` }}
            >
              <span className="text-white">01</span>
              <h3
                className={`uppercase text-white font-spartan tracking-widest py-5 cursor-pointer `}
                onClick={() => {
                  setMenu('overview');
                }}
              >
                Overview
              </h3>
            </div>
          ) : (
            <div className={`flex items-center gap-x-6 center pl-4 active`}>
              <span className="text-grey">01</span>
              <h3
                className={`uppercase text-white font-spartan tracking-widest py-5 cursor-pointer `}
                onClick={() => {
                  setMenu('overview');
                }}
              >
                Overview
              </h3>
            </div>
          )}
          {menu == 'structure' ? (
            <div
              className={`flex items-center gap-x-6 center pl-4`}
              style={{ backgroundColor: `${planet.color}` }}
            >
              <span className="text-white">02</span>
              <h3
                className={`uppercase text-white font-spartan tracking-widest py-5 cursor-pointer `}
                onClick={() => {
                  setMenu('structure');
                }}
              >
                Structure
              </h3>
            </div>
          ) : (
            <div className={`flex items-center gap-x-6 center pl-4 active`}>
              <span className="text-grey">02</span>
              <h3
                className={`uppercase text-white font-spartan tracking-widest py-5 cursor-pointer `}
                onClick={() => {
                  setMenu('structure');
                }}
              >
                Structure
              </h3>
            </div>
          )}
          {menu == 'surface' ? (
            <div
              className={`flex items-center gap-x-6 center pl-4`}
              style={{ backgroundColor: `${planet.color}` }}
            >
              <span className="text-white">02</span>
              <h3
                className={`uppercase text-white font-spartan tracking-widest py-5 cursor-pointer `}
                onClick={() => {
                  setMenu('surface');
                }}
              >
                Surface
              </h3>
            </div>
          ) : (
            <div className={`flex items-center gap-x-6 center pl-4 active`}>
              <span className="text-grey">03</span>
              <h3
                className={`uppercase text-white font-spartan tracking-widest py-5 cursor-pointer `}
                onClick={() => {
                  setMenu('surface');
                }}
              >
                Surface
              </h3>
            </div>
          )}
        </section>
        <section className="px-4 flex flex-col gap-y-2 pb-10 md:flex-row md:justify-between md:mt-10 info">
          <div className="flex justify-between px-6 py-3 bordered-square items-center md:w-44 md:flex-col md:py-6 md:items-start">
            <p className="text-grey uppercase font-spartan text-sm">
              Rotation Time
            </p>
            <span className="text-white text-xl md:text-3xl">
              {planet.rotation}
            </span>
          </div>
          <div className="flex justify-between px-6 py-3 bordered-square items-center md:w-44 md:flex-col md:py-6 md:place-items-start">
            <p className="text-grey uppercase font-spartan text-sm md:">
              Revolution Time
            </p>
            <span className="text-white text-xl md:text-3xl">
              {planet.revolution}
            </span>
          </div>
          <div className="flex justify-between px-6 py-3 bordered-square items-center md:w-44 md:flex-col md:py-6 md:items-start">
            <p className="text-grey uppercase font-spartan text-sm">Radius</p>
            <span className="text-white text-xl md:text-3xl">
              {planet.radius}
            </span>
          </div>
          <div className="flex justify-between px-6 py-3 bordered-square items-center md:w-44 md:flex-col md:py-6 md:md:items-start">
            <p className="text-grey uppercase font-spartan text-sm">
              Average Temp.
            </p>
            <span className="text-white text-xl md:text-3xl">
              {planet.temperature}
            </span>
          </div>
        </section>
      </article>
    </main>
  );
};
export default App;
