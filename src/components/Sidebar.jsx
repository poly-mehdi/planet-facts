import { useGlobalContext } from '../context';
import iconMenu from '../assets/icon-hamburger.svg';
import iconChevron from '../assets/icon-chevron.svg';
import data from '../data.json';

const Sidebar = () => {
  const { isSidebarOpen, setPlanet, toggleSidebar } = useGlobalContext();
  const handlePlanet = (item) => {
    document.body.style.overflow = 'auto';
    setPlanet(data[item]);
    toggleSidebar();
  };
  return (
    <aside
      className={`${isSidebarOpen ? 'sidebar show-sidebar' : 'sidebar'} z-20`}
    >
      <div className="px-4">
        {data.map((planet, item) => {
          return (
            <div
              className={`flex justify-between cursor-pointer py-4 ${
                item !== 7 ? 'bordered-bottom' : ''
              }`}
              key={item}
              onClick={() => handlePlanet(item)}
            >
              <div className="flex gap-x-4 items-center">
                <div
                  className={`h-5 w-5 rounded-2xl bg-grey`}
                  style={{ backgroundColor: `${planet.color}` }}
                ></div>
                <p className="text-white uppercase font-spartan tracking-widest font-medium text-xl">
                  {planet.name}
                </p>
              </div>
              <img src={iconChevron} alt="chevron" className="h-3" />
            </div>
          );
        })}
      </div>
    </aside>
  );
};
export default Sidebar;
