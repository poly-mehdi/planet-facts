import { createContext, useState, useContext } from 'react'
import data from './data.json'

const AppContext = createContext()

export const AppProvider = ({ children }) => {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false)
  const [planet, setPlanet] = useState(data[2])

  const toggleSidebar = () => {
    setIsSidebarOpen(!isSidebarOpen)
  }

  return (
    <AppContext.Provider
      value={{
        isSidebarOpen,
        toggleSidebar,
        planet,
        setPlanet,
      }}
    >
      {children}
    </AppContext.Provider>
  )
}

export const useGlobalContext = () => {
  return useContext(AppContext)
}
